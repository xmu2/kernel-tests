From 00a5994c70dcf674dfa85fda7aa0bbe9974b583e Mon Sep 17 00:00:00 2001
From: Jan Stancek <jstancek@redhat.com>
Date: Tue, 22 Oct 2024 11:14:02 +0200
Subject: [PATCH] read_all: limit sysfs tpm entries to single worker

Repeated reads from TPM entries (tcg_operations, vs_operations,
caps,..) are causing big delays with 3 or more repetitions,
which has signs of some kind of rate-limitting on firmware side.

This patch introduces a new kind of blacklist, which doesn't
completely skips the entry, but assigns it to only single
worker.

Signed-off-by: Jan Stancek <jstancek@redhat.com>
Reviewed-by: Cyril Hrubis <chrubis@suse.cz>
Reviewed-by: Li Wang <liwang@redhat.com>
Reviewed-by: Petr Vorel <pvorel@suse.cz>
---
 testcases/kernel/fs/read_all/read_all.c | 49 +++++++++++++++++++++----
 1 file changed, 41 insertions(+), 8 deletions(-)

diff --git a/testcases/kernel/fs/read_all/read_all.c b/testcases/kernel/fs/read_all/read_all.c
index 266678ea7..9c58b5e85 100644
--- a/testcases/kernel/fs/read_all/read_all.c
+++ b/testcases/kernel/fs/read_all/read_all.c
@@ -94,11 +94,17 @@ static int worker_timeout;
 static int timeout_warnings_left = 15;
 
 static char *blacklist[] = {
-	NULL, /* reserved for -e parameter */
+	"/reserved/", /* reserved for -e parameter */
 	"/sys/kernel/debug/*",
 	"/sys/devices/platform/*/eeprom",
 	"/sys/devices/platform/*/nvmem",
 	"/sys/*/cpu??*(?)/*",	/* cpu* entries with 2 or more digits */
+	NULL
+};
+
+static char *ratelimit_list[] = {
+	"/sys/devices/*/tpm*",
+	NULL,
 };
 
 static long long epoch;
@@ -193,19 +199,43 @@ static void sanitize_str(char *buf, ssize_t count)
 		strcpy(buf + MAX_DISPLAY, "...");
 }
 
-static int is_blacklisted(const char *path)
+static int is_onlist(const char *path, char *list[])
 {
-	unsigned int i;
+	unsigned int i = 0;
+
+	while (1) {
+		const char *pattern = list[i++];
 
-	for (i = 0; i < ARRAY_SIZE(blacklist); i++) {
-		if (blacklist[i] && !fnmatch(blacklist[i], path, FNM_EXTMATCH)) {
-			if (verbose)
-				tst_res(TINFO, "Ignoring %s", path);
+		if (!pattern)
+			break;
+		if (!fnmatch(pattern, path, FNM_EXTMATCH))
 			return 1;
-		}
 	}
 
 	return 0;
+
+}
+
+static int is_blacklisted(const char *path)
+{
+	int ret;
+
+	ret = is_onlist(path, blacklist);
+	if (ret && verbose)
+		tst_res(TINFO, "Ignoring %s", path);
+
+	return ret;
+}
+
+static int is_ratelimitted(const char *path)
+{
+	int ret;
+
+	ret = is_onlist(path, ratelimit_list);
+	if (ret && verbose)
+		tst_res(TINFO, "Limiting to single worker %s", path);
+
+	return ret;
 }
 
 static void worker_heartbeat(const int worker)
@@ -503,6 +533,9 @@ static int sched_work(const int first_worker,
 	int min_ttl = worker_timeout, sleep_time = 1;
 	int pushed, workers_pushed = 0;
 
+	if (is_ratelimitted(path))
+		repetitions = 1;
+
 	for (i = 0, j = first_worker; i < repetitions; j++) {
 		if (j >= worker_count)
 			j = 0;
-- 
2.47.0

